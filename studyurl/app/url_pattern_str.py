# 定義轉換用的代碼

class ThreeDigitConverter(object):
    regex = '[2-6]{3}'

    def to_url(self, value):
        return '%03d' % value
    
    def to_python(self, value):
        return int(value)