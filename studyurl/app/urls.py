from django.urls import path, re_path
from app.views import index, list, article

from .url_pattern_str import ThreeDigitConverter
from django.urls import register_converter

register_converter(ThreeDigitConverter, 'threeint')

urlpatterns = [
    path("indexs/", index, name='index666'), # 善用name參數，未來改url不會在html又改
    path('list/', list, name='list'),
    path('article/<int:id>/', article, name='article'),          # 只匹配數字 
    # # path('article/<str:id>/', article, name='article'),       # 匹配中文 英文 空格 符號
    path('article/<slug:id>/', article, name='article'),         # 匹配數字 字母
    # # path('article/<path:id>/', article, name='article'),        # 匹配中文 英文  空格 符號 數字 /
    # re_path('article/([5-9]{4})/$', article, name='article'),    #正則表達django1
    # re_path('article/(?P<id>[2-5]{2})/$', article, name='article'),
    path('article/<threeint:id>/', article, name='article'),          # 只匹配數字 

]