from django.shortcuts import render, HttpResponse

# Create your views here.
def index(request):
    return HttpResponse('index page!')

def list(request):
    return render(request, 'list.html')

def article(request, id):
    types = type(id)
    print('值: {},類型 : {}'.format(id, types))
    return render(request, 'app/article.html', {'id': id})